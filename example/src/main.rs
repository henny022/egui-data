use eframe::egui;
use egui_data::EguiData;
use egui_data_derive::EguiData;

fn main() {
    let native_options = eframe::NativeOptions::default();
    eframe::run_native(
        "My egui App",
        native_options,
        Box::new(|cc| Box::new(MyEguiApp::new(cc))),
    )
    .expect("failed to run app");
}

#[derive(EguiData)]
struct U;

#[derive(EguiData)]
struct D {
    number: i32,
    #[hidden]
    unit: U,
    #[labels(&["a", "b", "c"])]
    vec: Vec<i8>,
    raw: [u8; 5],
}

#[derive(Default)]
struct MyEguiApp {}

impl MyEguiApp {
    fn new(_cc: &eframe::CreationContext<'_>) -> Self {
        Self::default()
    }
}

impl eframe::App for MyEguiApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.heading("Hello World!");
            let mut d = D {
                number: 3,
                unit: U,
                vec: vec![1, 2, 3],
                raw: [1, 2, 3, 4, 5],
            };
            d.ui(ui, "d");
        });
    }
}
