use std::str::FromStr;

use egui;
pub trait EguiData {
    fn ui(&mut self, ui: &mut egui::Ui, label: &str) {
        self.ui_inner(ui, label)
    }
    fn ui_labeled(&mut self, ui: &mut egui::Ui, label: &str, _sublabels: &[&str]) {
        EguiData::ui(self, ui, label)
    }
    fn ui_inner(&mut self, ui: &mut egui::Ui, label: &str);
}

impl EguiData for bool {
    fn ui_inner(&mut self, ui: &mut egui::Ui, label: &str) {
        ui.checkbox(self, label);
    }
}

impl EguiData for String {
    fn ui_inner(&mut self, ui: &mut egui::Ui, label: &str) {
        ui.horizontal(|ui| {
            ui.label(label);
            ui.text_edit_singleline(self);
        });
    }
}

impl<T: EguiData, const N: usize> EguiData for [T; N] {
    fn ui(&mut self, ui: &mut egui::Ui, label: &str) {
        ui.collapsing(label, |ui| {
            self.ui_inner(ui, label);
        });
    }

    fn ui_labeled(&mut self, ui: &mut egui::Ui, label: &str, sublabels: &[&str]) {
        ui.collapsing(label, |ui| {
            for (ele, label) in self.iter_mut().zip(sublabels.iter()) {
                ele.ui(ui, label);
            }
        });
    }

    fn ui_inner(&mut self, ui: &mut egui::Ui, _label: &str) {
        for (i, ele) in self.iter_mut().enumerate() {
            ele.ui(ui, &format!("{}", i));
        }
    }
}

impl<T: EguiData> EguiData for Vec<T> {
    fn ui(&mut self, ui: &mut egui::Ui, label: &str) {
        ui.collapsing(label, |ui| {
            self.ui_inner(ui, label);
        });
    }

    fn ui_labeled(&mut self, ui: &mut egui::Ui, label: &str, sublabels: &[&str]) {
        ui.collapsing(label, |ui| {
            for (ele, label) in self.iter_mut().zip(sublabels.iter()) {
                ele.ui(ui, label);
            }
        });
    }

    fn ui_inner(&mut self, ui: &mut egui::Ui, _label: &str) {
        for (i, ele) in self.iter_mut().enumerate() {
            ele.ui(ui, &format!("{}", i));
        }
    }
}

fn text_ui<T: FromStr + ToString>(value: &mut T, ui: &mut egui::Ui, label: &str) {
    let mut text = value.to_string();
    ui.horizontal(|ui| {
        ui.label(label);
        ui.text_edit_singleline(&mut text);
    });
    if let Ok(v) = text.parse::<T>() {
        *value = v;
    }
}

impl EguiData for i8 {
    fn ui_inner(&mut self, ui: &mut egui::Ui, label: &str) {
        text_ui(self, ui, label)
    }
}

impl EguiData for i16 {
    fn ui_inner(&mut self, ui: &mut egui::Ui, label: &str) {
        text_ui(self, ui, label)
    }
}

impl EguiData for i32 {
    fn ui_inner(&mut self, ui: &mut egui::Ui, label: &str) {
        text_ui(self, ui, label)
    }
}

impl EguiData for i64 {
    fn ui_inner(&mut self, ui: &mut egui::Ui, label: &str) {
        text_ui(self, ui, label)
    }
}

impl EguiData for i128 {
    fn ui_inner(&mut self, ui: &mut egui::Ui, label: &str) {
        text_ui(self, ui, label)
    }
}

impl EguiData for u8 {
    fn ui_inner(&mut self, ui: &mut egui::Ui, label: &str) {
        text_ui(self, ui, label)
    }
}

impl EguiData for u16 {
    fn ui_inner(&mut self, ui: &mut egui::Ui, label: &str) {
        text_ui(self, ui, label)
    }
}

impl EguiData for u32 {
    fn ui_inner(&mut self, ui: &mut egui::Ui, label: &str) {
        text_ui(self, ui, label)
    }
}

impl EguiData for u64 {
    fn ui_inner(&mut self, ui: &mut egui::Ui, label: &str) {
        text_ui(self, ui, label)
    }
}

impl EguiData for u128 {
    fn ui_inner(&mut self, ui: &mut egui::Ui, label: &str) {
        text_ui(self, ui, label)
    }
}

impl EguiData for f32 {
    fn ui_inner(&mut self, ui: &mut egui::Ui, label: &str) {
        text_ui(self, ui, label)
    }
}

impl EguiData for f64 {
    fn ui_inner(&mut self, ui: &mut egui::Ui, label: &str) {
        text_ui(self, ui, label)
    }
}
