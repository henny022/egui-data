use proc_macro::TokenStream;
use quote::{quote, quote_spanned};
use syn::{
    parse_macro_input, parse_quote, spanned::Spanned, Attribute, DeriveInput, Field, GenericParam,
    Generics, Index,
};

#[proc_macro_derive(EguiData, attributes(labels, hidden))]
pub fn derive_egui_data(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    let name = input.ident;

    let generics = add_trait_bounds(input.generics);
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();

    let children = match input.data {
        syn::Data::Struct(ref data) => match data.fields {
            syn::Fields::Named(ref fields) => {
                let recurse = fields.named.iter().filter_map(|f| {
                    let name = &f.ident;

                    let hidden = find_attr(f, "hidden");
                    if hidden.is_some() {
                        return None;
                    }
                    // find 'labels' attribute
                    let labels = find_attr(f, "labels");

                    if let Some(labels) = labels {
                        let labels = &labels
                            .meta
                            .require_list()
                            .expect("bad labels attribute")
                            .tokens;
                        Some(quote_spanned! {f.span() =>
                            egui_data::EguiData::ui_labeled(
                                &mut self.#name,
                                ui, stringify!(#name),
                                #labels);
                        })
                    } else {
                        Some(quote_spanned! {f.span() =>
                            egui_data::EguiData::ui(&mut self.#name, ui, stringify!(#name));
                        })
                    }
                });
                quote! {
                    #(#recurse)*
                }
            }
            syn::Fields::Unnamed(ref fields) => {
                let recurse = fields.unnamed.iter().enumerate().map(|(i, f)| {
                    let index = Index::from(i);
                    quote_spanned! {f.span() =>
                        egui_data::EguiData::ui(&mut self.#index, ui, stringify!(#index));
                    }
                });
                quote! {
                    #(#recurse)*
                }
            }
            syn::Fields::Unit => quote!(ui.label("()");),
        },
        syn::Data::Enum(_) => unimplemented!(),
        syn::Data::Union(_) => unimplemented!(),
    };

    let expanded = quote! {
        impl #impl_generics egui_data::EguiData for #name #ty_generics #where_clause {
            fn ui(&mut self, ui: &mut egui::Ui, label: &str) {
                ui.collapsing(label, |ui| {
                    self.ui_inner(ui, label);
                });
            }

            fn ui_inner(&mut self, ui: &mut egui::Ui, _label: &str) {
                #children
            }
        }
    };
    TokenStream::from(expanded)
}

fn find_attr<'a>(field: &'a Field, name: &str) -> Option<&'a Attribute> {
    field.attrs.iter().find(|attr| attr.path().is_ident(name))
}

// Add a bound `T: HeapSize` to every type parameter T.
fn add_trait_bounds(mut generics: Generics) -> Generics {
    for param in &mut generics.params {
        if let GenericParam::Type(ref mut type_param) = *param {
            type_param.bounds.push(parse_quote!(egui_data::EguiData));
        }
    }
    generics
}
